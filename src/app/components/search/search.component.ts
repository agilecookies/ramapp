import { EventEmitter } from '@angular/core';
import { Component, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Output () search: EventEmitter<string> = new EventEmitter();

  public searchField: FormControl;
  constructor() {
    this.searchField = new FormControl();
  }

  ngOnInit() {
    this.searchField.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      startWith('')
      ).subscribe((data => {
        this.search.emit(data);
      }));
  }

}
