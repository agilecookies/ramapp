import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CharacterService } from 'src/app/services/character.service';

import { Character } from './../../models/character.interface';


@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  public characterId: number;
  public character: Character;
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private characterSercvice: CharacterService
    ) { }

  ngOnInit(): void {
    this.getCharacter();
  }

  getCharacter(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.characterSercvice.getCharacter(id)
    .subscribe(character => this.character = character);
  }

  goBack(): void {
    this.location.back();
  }
}
