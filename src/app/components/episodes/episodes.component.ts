import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.scss']
})
export class EpisodesComponent {
  @Input() episodes: any;
  public panelOpenState: boolean;

  constructor() { }

}
