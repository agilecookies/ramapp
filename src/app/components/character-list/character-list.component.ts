import { Component, Input } from '@angular/core';
import { Character } from 'src/app/models/character.interface';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent {
  @Input() characterList: [Character];

  constructor() { }

}
