import { Component, OnDestroy, OnInit } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { Character } from './../../models/character.interface';
import { CharacterService } from './../../services/character.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  public finalCharacters: any = [];
  public search$  = new Subject<string>();
  public characters$: Observable<any>;
  public isAlive = true;

  constructor(private characterService: CharacterService) { }

  ngOnInit() {
    this.characters$ = this.getCharacters();
    combineLatest([this.search$, this.characters$]).pipe(
      takeWhile(() => this.isAlive = true))
      .subscribe(data => {
        const search = data[0];
        const characters = data[1].results;
        if (!search) {
          this.finalCharacters = characters;
        } else {
          this.finalCharacters = characters.filter((character) => character.name.toLowerCase().indexOf(search.toLocaleLowerCase()) !== -1);
        }
      });
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

  getCharacters(): Observable<Character[]> {
    return this.characterService.getCharacters();
  }
}
