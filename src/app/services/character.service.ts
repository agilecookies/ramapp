import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { empty, Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from './../../environments/environment';
import { Character } from './../models/character.interface';



@Injectable({
  providedIn: 'root'
})

export class CharacterService {
public searchResults: any;

  constructor(private http: HttpClient) { }

  getCharacters(): Observable<any> {
    return this.http.get(`${environment.API_URL}`);
  }
  getCharacter(id): Observable<Character> {
    return this.http.get<Character>(`${environment.API_URL}/${id}`);
  }
}
